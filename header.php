<?php
$languages = pll_the_languages(['raw'=>1]);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,500,600,700|Syncopate:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <?php wp_head();?>
</head>
<body <?php body_class();?>>



  <div class="fixed-top">
    <div class="pre-navbar">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="d-inline-block">
              <ul class="list list-inline d-inline">
                <li class="list-inline-item"> <i class="fas fa-search"></i> <?php pll_e('Ricerca avanzata')?></li>
                <li class="list-inline-item">|</li>
                <li class="list-inline-item"> <a href="#" target="_blank"> <i class="fab fa-instagram"></i> </a> </li>
                <li class="list-inline-item"> <a href="#" target="_blank"> <i class="fab fa-facebook"></i> </a> </li>
                <li class="list-inline-item"> <a href="https://it.linkedin.com/company/cantini-vetro" target="_blank"> <i class="fab fa-linkedin"></i> </a> </li>
                <li class="list-inline-item">|</li>

              </ul>
              <div class="d-inline-block language-switcher">
                <ul class="list list-inline d-inline-block">
                  <?php foreach($languages as $language){
                    ?>
                    <li class="d-inline-block <?php echo (in_array('current-lang',$language['classes']) ? 'active' : '') ?>">
                      <a href="<?php echo $language['url']?>"><?php echo $language['slug']?></a>
                    </li>
                    <?php
                  }?>
                </ul>
              </div>
              <div class="d-inline-block">
                <a href="tel:+39 0571 656665">+39 0571 656665</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-gradient-dark">

      <nav class="navbar navbar-expand-lg navbar-dark">
        <img src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-small.png" height="50px;" class="d-inline-block align-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-top-menu-ita" aria-controls="menu-top-menu-ita" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <?php
        wp_nav_menu([
          'theme_location'=>'top-menu',
          'menu_class' => 'navbar-nav nav-fill w-100 align-items-star collapse navbar-collapse',
          'walker' => new wp_bootstrap_navwalker(),
          'container' => true
        ])
        ?>
      </nav>
    </div>
  </div>
