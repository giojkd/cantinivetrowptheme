<?php get_header();?>
<?php
$categoriesIcons = [
  [
    'label' => 'Vini',
    'icon' => 'vini',
  ],
  [
    'label' => 'Birra',
    'icon' => 'birra',
  ],
  [
    'label' => 'Distillati',
    'icon' => 'distillati',
  ],
  [
    'label' => 'Spumanti',
    'icon' => 'spumanti',
  ],
  [
    'label' => 'Acqua',
    'icon' => 'acqua',
  ],
  [
    'label' => 'Olio e Aceto',
    'icon' => 'olio-e-aceto',
  ],
  [
    'label' => 'Alimentare',
    'icon' => 'alimentare',
  ],
  [
    'label' => 'Cosmesi',
    'icon' => 'cosmesi',
  ]
];
$langIso = pll_current_language();

?>
<div class="container-fluid pl-0 pr-0 page-section background-cover" style="background-image:url('<?php echo get_template_directory_uri()?>/css/images/home-pic-cover.jpg')">
  <div class="row">
    <div class="col-md-12 text-center pt-4">
      <img class="mt-4" src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-medium.png" alt="">
    </div>
  </div>
  <div class="row homepage-icons">
    <?php foreach($categoriesIcons as $categoryIcon){
      ?>
      <div class="col text-center ">
        <p>
          <a href="#page-section-<?php echo $categoryIcon['icon']?>">
            <img src="<?php echo get_template_directory_uri()?>/css/images/icon-<?php echo $categoryIcon['icon']?>.png" alt=""><br>
            <?php echo pll_e($categoryIcon['label'])?>
          </a>
        </p>
      </div>
      <?php
    }?>

  </div>
</div>
<?php foreach($categoriesIcons as $index => $categoryIcon){
  ?>
  <div class="container-fluid pl-0 pr-0 page-section" id="page-section-<?php echo $categoryIcon['icon']?>">
    <div class="page-section-header">
      <p><?php echo pll_e($categoryIcon['label'])?> <img src="<?php echo get_template_directory_uri()?>/css/images/icon-arrow-right.png" alt=""> </p>
    </div>
    <div class="twentytwenty twentytwenty-container">
      <div class="tt-image-wrapper" style="background:url('<?php echo get_template_directory_uri()?>/css/images/home-pic-<?php echo $index?>-0.jpg')"></div>
      <div class="tt-image-wrapper" style="background:url('<?php echo get_template_directory_uri()?>/css/images/home-pic-<?php echo $index?>-1.jpg')">
        <div class="section-tooltip" data-top="30" data-left="70">
          <div class="tooltip-trigger">
            <a href="javascript:"><i class="fas fa-plus"></i></a>
          </div>
          <div class="tooltip-content d-none">

          </div>
        </div>
      </div>
    </div>
  </div>
<?php }?>

<?php get_footer();?>
