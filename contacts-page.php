<?php /* Template Name: Contacts */ ?>
<?php get_header();?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?php the_title(); ?></h1>
      <div class="contacts">
        <h2>Richiedi informazioni</h2>
        <form class="" action="index.html" method="post">
          <div class="row">
            <div class="col-md-6">
              <input type="text" name="data[name]" value="" class="form-control" placeholder="Nome">
            </div>
            <div class="col-md-6">
              <input type="text" name="data[surname]" value="" class="form-control" placeholder="Cognome">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <input type="text" name="data[email]" value="" class="form-control" placeholder="Email">
            </div>
            <div class="col-md-6">
              <input type="text" name="data[telephone]" value="" class="form-control" placeholder="Telefono">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <input type="text" name="data[company]" value="" class="form-control" placeholder="Ragione sociale">
            </div>
            <div class="col-md-6">
              <input type="text" name="data[vat_number]" value="" class="form-control" placeholder="Partita iva">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <textarea name="name" rows="8" cols="80" class="form-control" placeholder="Richiesta"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-block btn-cantini" name="button">Invia richiesta</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="contacts-directions">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    the_content();
  endwhile; else: ?>
  <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
</div>
</div>

<?php get_footer();?>
