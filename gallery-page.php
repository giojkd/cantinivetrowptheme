<?php /* Template Name: Gallery */ ?>
<?php

$result = array();
$galleryDir = get_template_directory().'/gallery';
$folders = preg_grep('/^([^.])/',scandir($galleryDir));
$data = [];
foreach ($folders as  $folder)
{
  if (!in_array($folder,array(".","..")))
  {
    $data[$folder] = [
      'cfg' => json_decode(file_get_contents(implode('/',[$galleryDir,$folder,'cfg.json'])),1),
      'photos' => array_slice(preg_grep('/^([^.])/', scandir(implode('/',[$galleryDir,$folder,'photos']))),2),
      'path_prefix' => implode('/',[get_template_directory_uri(),'gallery',$folder,'photos'])
    ];
  }
}

get_header();?>

<script type="text/javascript">
var slides = <?php echo json_encode($data);?>;

</script>
<div class="navbar-spacer"></div>
<div class="page-wrapper" style="background:url('<?php echo get_template_directory_uri()?>/css/images/bg-big-logo.png')">


  <div class="gallery-wrapper">
    <div class="main-carousel">

      <?php foreach($data as $index =>  $cat){
        ?>
        <div class="carousel-cell" data-cat="<?php echo $index?>">
          <div class="carousel-cell-info" onclick="showBigGallery()">
            <div class="stack-expander rotate-180">
              <i class="fa fa-arrow-up"></i>
            </div>
            <h2><?php echo $cat['cfg']['text']['name']['it']?></h2>
          </div>
          <div class="carousel-cell-image">
            <img class="" src="<?php echo implode('/',[$cat['path_prefix'],$cat['photos'][0]])?>" alt="">
          </div>
        </div>
      <?php } ?>

    </div>
  </div>

  <div class="fullpage-gallery d-none">

    <div class="row">
      <div class="col-md-4 offset-md-4 text-center">
        <a href="javascript:" onclick="hideBigGallery()" class="close-btn"> <i class="fa fa-arrow-down"></i> </a>
        <ul class="list list-unstyled">

        </ul>
      </div>
    </div>
  </div>
</div>

<?php get_footer();?>
