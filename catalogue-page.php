<?php /* Template Name: Catalogue */ ?>
<?php get_header();?>
<div style="height:100px"></div>
<div class="container-fluid">
  <div class="row p-0">
    <div class="col-md-12 p-0">
      <div class="page-cover background-cover d-flex justify-content-center" style="background-image:url('<?php echo get_template_directory_uri()?>/css/images/catalogue-pic-0.jpg')">
        <div class="text-center align-self-center">
          <img src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-big.png" width="240" alt=""><br>
          <img src="<?php echo get_template_directory_uri()?>/css/images/glass-design.png" width="240" alt="">
        </div>
      </div>

    </div>
  </div>
</div>
<div id="app">
  <div class="container catalogue-filters">
    <div class="text-center">
      <h2>Ricerca avanzata prodotti</h2>
      <img src="<?php echo get_template_directory_uri()?>/css/images/icon-arrow-down-green.png">
    </div>
    <div class="row">
      <div class="col-md-4 text-center" v-for="attribute in c.attributes">
        <select class="form-control select2" :id="'attributes-select-'+attribute.id" v-model="c.filters.selected_attributes" multiple :data-placeholder="attribute.texts.name">
          <option value="" v-for="value in attribute.values" v-if="typeof(value.texts) != 'undefined'" :value="value.id">{{value.texts.name}}</option>
        </select>
      </div>
    </div>
    <div class="white-separator"></div>
    <div class="row mb-4">
      <div class="col-md-3">
        <div class="filters-holder">
          <h3>Categorie</h3>
          <ul class="list-unstyled column-filters" v-for="attribute in c.attributes" v-if="attribute.id==2">
            <li v-for="value in attribute.values" onclick="$(this).toggleClass('active')" @click="toggleAttribute(value.id)">{{value.texts.name}}</li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        <div class="row products-list" v-if="c.products.length > 0" v-for="(products,index) in c.products">
          <div @click="selectProduct(index,index_inner,product.id)" class="col-md-4 product-item text-center" :class="{'d-none': product.hide }" :id="'product-'+product.id" v-for="(product,index_inner) in products">
            <div class="product-item-img">
              <img :src="product.versions[0].photos[0]" alt="">
            </div>
            <p class="product-item-category">{{product.category}}</p>
            <p class="product-item-name">{{product.name}}</p>
            <p class="product-item-cta">Scheda tecnica prodotto Richiedi info e campionatura</p>
            <img src="<?php echo get_template_directory_uri()?>/css/images/icon-arrow-right.png" class="product-item-arrow-down d-none rotate-90">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="selected-product col-md-12" :class="{'d-none':c.hideProductDetail}">

    <!-- Modal -->
    <div class="modal fade" id="infoRequestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Invia una richiesta di informazioni per {{c.selected_product.name}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body contacts">
            <form class="" action="index.html" method="post">
              <div class="row">
                <div class="col-md-6">
                  <input type="text" name="data[name]" value="" class="form-control" placeholder="Nome">
                </div>
                <div class="col-md-6">
                  <input type="text" name="data[surname]" value="" class="form-control" placeholder="Cognome">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" name="data[email]" value="" class="form-control" placeholder="Email">
                </div>
                <div class="col-md-6">
                  <input type="text" name="data[telephone]" value="" class="form-control" placeholder="Telefono">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" name="data[company]" value="" class="form-control" placeholder="Ragione sociale">
                </div>
                <div class="col-md-6">
                  <input type="text" name="data[vat_number]" value="" class="form-control" placeholder="Partita iva">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <textarea name="name" rows="8" cols="80" class="form-control" placeholder="Richiesta"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-block btn-cantini" name="button">Invia richiesta</button>
                </div>
              </div>
            </form>
          </div>
          <!--
          <div class="modal-footer">
            <button type="button" class="btn btn-cantini btn-block">Invia</button>
          </div>
          -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <h1>{{c.selected_product.name}}</h1>
        <h2>{{c.selected_product.category}}</h2>
      </div>
      <div class="col-md-4 text-right">

        <img src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-small.png" height="80" alt=""><br>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h3>Caratteristiche</h3>
        <div v-for="attribute in c.selected_product.versions[c.selected_version_nth].attributes" class="attribute-wrapper">
          <span class="attribute-name">{{attribute.texts.name}}</span>
          <span class="attribute-value" v-for="value in attribute.values">{{value.texts.name}}</span>
        </div>
      </div>
      <div class="col-md-6 text-center image-wrapper">
        <!--<img :src="photo" alt="" v-for="photo in c.selected_product.versions[c.selected_version_nth].photos">-->
        <div class='modelo-wrapper'><div style="width: 100%; padding-bottom: 56.25%; position: relative"> <div style="position: absolute; top: 0; bottom: 0; left: 0; right: 0;"> <iframe src="https://app.modelo.io/embedded/8gQc4p7Sws?viewport=false&autoplay=false" style="width:100%;height:640px" frameborder="0" mozallowfullscreen webkitallowfullscreen allowfullscreen ></iframe> </div> </div> </div>


        <a href="javascript:" @click="closeProductDetail()" > <i class="fas fa-times fa-2x"></i> </a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h3>Capacità</h3>
        <ul class="list-inline">
          <li v-for="version in c.selected_product.versions" class="list-inline-item version-name">
            <a @click="select_version(version.index)" class="social-icon text-xs-center" href="javascript:">{{version.name}}</a>
          </li>
        </ul>
      </div>
      <div class="col-md-6 pt-2 text-center product-links">
        <a :href="file" download target="_blank" v-for="file in c.selected_product.versions[c.selected_version_nth].tech_sheets"><i class="far fa-file-alt"></i> Scarica la scheda prodotto</a><br>
        <a href="javascript:" @click="showRequestModal()">Richiedi informazioni e campionatura</a>
      </div>
    </div>


  </div>
</div>







<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />


<?php get_footer();?>
