<?php /* Template Name: Design */ ?>
<?php get_header();?>

<div class="container-fluid">
  <div class="row p-0">
    <div class="col-md-12 p-0">
      <div class="page-cover background-cover d-flex justify-content-center" style="background-image:url('<?php echo get_template_directory_uri()?>/css/images/design-pic-0.jpg')">
        <div class="text-center align-self-center">
          <img src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-big.png" width="240" alt=""><br>
          <img src="<?php echo get_template_directory_uri()?>/css/images/glass-design.png" width="240" alt="">
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
      the_content();
    endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
  <?php endif; ?>
</div>
</div>
</div>


<?php get_footer();?>
