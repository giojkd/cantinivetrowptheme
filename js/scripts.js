$(function(){
  $.scrollify({
    section : ".page-section",
  });
  //$(".navbar.fixed-top").autoHidingNavbar();
  $('.page-section').each(function(){
    var padder = $('<div style="height:100px"></div>');
    $(this).prepend(padder);
  })

  setTtImageWrapperHeight();
  $(window).resize(function() {
    setTtImageWrapperHeight();
  });
  $('.section-tooltip').each(function(){
    $(this).animate({'top':$(this).data('top')+'%','left':$(this).data('left')+'%'}, {
    duration: 500})
  })
  $('.section-tooltip').hover(function(){
    $(this).find('.tooltip-content').removeClass('d-none');
  },function(){
    $(this).find('.tooltip-content').addClass('d-none');
  })
  $('.tooltip-trigger').hover(function(){
    $(this).closest('.section-tooltip').find('.tooltip-content').removeClass('d-none')
  })
  $('.page-slice').each(function(){
    var bg = String($(this).css('background-image'));
    //bg = bg.replace('wp-content','cantinivetro/wp-content');
    console.log(bg);
    $(this).css({'background-image':bg});
  })
  $(window).scroll(function(){
    if($(this).scrollTop() > 400 ){
      $('#topBtn').fadeIn()
    }else{
      $('#topBtn').fadeOut()
    }
  })
  $('#topBtn').click(function(){
    $('html, body').animate({scrollTop:0},600);
  })
})

function setTtImageWrapperHeight(){
  $('.tt-image-wrapper').css({height:$( window ).height()+'px'});
  $(".twentytwenty").twentytwenty({
    no_overlay:true,
    click_to_move: false
  });
}
