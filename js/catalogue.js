var selectedProductDefault = {
  versions:[
    {
      attributes:[]
    }
  ]
};
var apiPrefix = 'http://cantinicatalogue.brandboosterdemo.com/api/';
var lang = 'it';
var app = new Vue({
  el: '#app',
  data: {
    c:{
      attributes:[],
      products:[],
      hideProductDetail:true,
      selected_product:selectedProductDefault,
      filters:{
        selected_attributes:[],
      },
      selected_version_nth:0
    }
  },
  methods:{
    showRequestModal(){
      $('#infoRequestModal').modal('show');
    },
    toggleAttribute(id){
      var found = -1;
      for (var i in app.c.filters.selected_attributes) {
        if(app.c.filters.selected_attributes[i] == id){
          found = i;
          app.c.filters.selected_attributes.splice(i, 1);
        }
      }
      if(found == -1){
        app.c.filters.selected_attributes.push(id)
      }
      app.filterProducts();
    },
    closeProductDetail(){
      app.c.selected_product = selectedProductDefault;
      app.c.hideProductDetail = true;
      $('.product-item-arrow-down').addClass('d-none');
    },
    select_version(id){
      this.c.selected_version_nth = id;
    },
    filterProducts(){
      var countFilters = app.c.filters.selected_attributes.length;
      var sf = app.c.filters.selected_attributes;
      var chunks = app.c.products;
      var countMatches = 0;
      var hide = false;
      for(var w in chunks){
        var products = chunks[w];
        for(var i in products){
          var p = products[i];
          for(var j in app.c.filters.selected_attributes){
            if(p.attribute_values_ids.includes(sf[j])){
              countMatches++;
            }
          }
          if(countMatches == countFilters){
            hide = false;
          }else{
            hide = true;
          }
          /*
          if(countMatches == 0){
          hide = true;
        }else{
        hide = false;
      }
      */
      app.c.products[w][i].hide = hide;
      countMatches = 0;
    }
  }
  var npa = [];
  for(var w in chunks){
    for(var k in chunks[w]){
      npa.push(chunks[w][k])
    }
  }
  var temparray = [];
  var chunks_ = [];
  var chunk = 3;
  npa.sort(function(x,y){
    return (x.hide === y.hide)? 0 : x.hide? -1 : 1;
  });
  npa.reverse();
  for (i=0,j=npa.length; i<j; i+=chunk) {
    temparray = npa.slice(i,i+chunk);
    chunks_.push(temparray);
  }
  app.c.products = chunks_;
},
selectProduct(index,id,product_id){
  console.log('index: '+index+' id: '+id);
  this.c.hideProductDetail = false;
  this.c.selected_version_nth = 0;
  this.c.selected_product = this.c.products[index][id];
  $('#product-'+product_id).closest('.row').append($('.selected-product'));
  $('.product-item-arrow-down').addClass('d-none');
  $('#product-'+product_id).find('.product-item-arrow-down').removeClass('d-none');
},
loadProducts(){
  axios
  .post(apiPrefix+'products',this.filters)
  .then(function (response) {
    app.c.products = response.data;
  })
},
loadAttributes(){
  axios
  .post(apiPrefix+'attributes')
  .then(function (response) {
    console.log(response);
    app.c.attributes = response.data;
    setTimeout(function(){
      $('.select2').select2();
      $('.select2').change(function(){
        app.closeProductDetail();
        app.c.filters.selected_attributes = [];
        $('.select2').each(function(){
          var arrayVal = [];
          arrayVal = $(this).val();
          if(arrayVal.length>0){
            for(var i in arrayVal){
              app.c.filters.selected_attributes.push(parseInt(arrayVal[i]))
            }
          }
        })
        app.filterProducts()
      });
    },'500');
  });
}
},
created(){

  this.loadProducts();
  this.loadAttributes();
},
mounted(){

},
watch:{

},
computed:{},

})
