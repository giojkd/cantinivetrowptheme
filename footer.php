<?php wp_footer();?>
<div class="container footer">
  <div class="row ">
    <div class="col-md-12 text-center">
      <img src="<?php echo get_template_directory_uri()?>/css/images/logo-cantini-medium.png" alt="">
      <h3><?php pll_e('Contattaci'); ?></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 footer-icon text-center">
      <img src="<?php echo get_template_directory_uri()?>/css/images/icon-address.png" alt="">
      <p><?php pll_e('Indirizzo')?></p>
      <ul class="list list-unstyled">
        <li>Via delle Città, 62 50052</li>
        <li>Certaldo Firenze (Italy)</li>
      </ul>
    </div>
    <div class="col-md-3 footer-icon text-center">
      <img src="<?php echo get_template_directory_uri()?>/css/images/icon-mail.png" alt="">
      <p><?php pll_e('Mail')?></p>
      <ul class="list list-unstyled">
        <li>cantinivetro@cantinivetro.it</li>
        <li>cantinivetrosrl@legalmail.it</li>
      </ul>
    </div>
    <div class="col-md-3 footer-icon text-center">
      <img src="<?php echo get_template_directory_uri()?>/css/images/icon-phone.png" alt="">
      <p><?php pll_e('Telefono e Fax')?></p>
      <ul class="list list-unstyled">
        <li>Tel. +39 0571 656665 </li>
        <li>Tel. +39 0571 656672 </li>
        <li>Fax +39 0571 662857</li>
      </ul>
    </div>
    <div class="col-md-3 footer-icon text-center">
      <img src="<?php echo get_template_directory_uri()?>/css/images/icon-clock.png" alt="">
      <p><?php pll_e('Ritiro Merce')?></p>
      <ul class="list list-unstyled">
        <li>Tel. +39 0571 656665 </li>
        <li>Tel. +39 0571 656672 </li>
        <li>Fax +39 0571 662857</li>
      </ul>
    </div>
  </div>

</div>

<div class="container-fluid mt-4">
  <div class="row p-0">
    <div class="col-md-12">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2892.516937913134!2d11.055640215723672!3d43.53326516819554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a3f3a5f927f29%3A0xbe68a526e461d1c0!2sCantini+Vetro!5e0!3m2!1sit!2sit!4v1559406398791!5m2!1sit!2sit" width="100%" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
</div>
<div class="container mt-4">
  <div class="row">
    <?php
    wp_nav_menu([
      'theme_location'=>'top-menu',
      'menu_class' => 'footer-menu',
      'container' => false
    ])
    ?>
  </div>
</div>
<div class="container post-footer">
  <div class="row">
    <div class="col-md-12 text-center">
      <span class="text-muted">© Created By <i class="fas fa-heart"></i></span> <a href="http://brandbooster.it/">Brand Booster | Empoli</a>
    </div>
  </div>
</div>
<div id="topBtn"><i class="fa fa-arrow-up"></i></div>
</body>
</html>
